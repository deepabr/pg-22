#include<stdio.h>
#include<math.h>
int input();
float distance(int,int,int,int);
void output(int,int,int,int,float);
int main()
{
	int x1,y1,x2,y2;
    float d;
	x1=input();
	y1=input();
    x2=input();
    y2=input();
	d=distance(x1,y1,x2,y2);
	output(x1,y1,x2,y2,d);
	return 0;
}
  int input()
  {
  	int a;
  printf("Enter the coordinate\n");
  scanf("%d",&a);
  return a;
  }
  float distance(int x1,int y1,int x2,int y2)
  {
  	float d;
  	d=(float)sqrt((pow(x2-x1,2))+(pow(y2-y1,2)));
  	return d;
  }
  void output(int x1 ,int y1 ,int x2,int y2,float d)
  {
  	printf("\n Distance between the two points is %f",d);
  }