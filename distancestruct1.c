#include<stdio.h>
#include<math.h>
struct point
{
    float x;
    float y;
};
typedef struct point Point;
Point input()
{
    Point p;
    printf("Enter a point\n");
    scanf("%f %f",&p.x,&p.y);
    return p;
}
 float distance(Point p1,Point p2)
 {
     float d;
     d=sqrt((pow(p2.x-p1.x,2))+(pow(p2.y-p1.y,2)));
     return d;
 }
 void output(Point p1,Point p2,float d)
 {
     printf("Distance between points %f,%f and %f,%f is %f\n",p1.x,p1.y,p2.x,p2.y,d);
 }
 int main()
 {
     Point p1,p2;
     float d;
     p1=input();
     p2=input();
     d=distance(p1,p2);
     output(p1,p2,d);
     return 0;
 }
 